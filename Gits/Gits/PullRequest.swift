//
//  PullRequest.swift
//  Gits
//
//  Created by Diego Cavalcante on 20/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import Foundation


struct PullRequest {
    
    init() {}
    
    init?(data : Any) {
        
        if let json = data as? JSON, let user = json["user"] as? JSON {
            print(user)
        }else {
            return nil
        }
    }
}
