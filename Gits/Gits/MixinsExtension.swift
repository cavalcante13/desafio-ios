//
//  Mixins.swift
//  Gits
//
//  Created by Diego Cavalcante on 20/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import Foundation
import UIKit

public typealias JSON = [String : Any]
