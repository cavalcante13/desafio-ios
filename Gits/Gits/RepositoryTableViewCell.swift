//
//  RepositoryTableViewCell.swift
//  Gits
//
//  Created by Diego Cavalcante on 19/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameRepository: UILabel?
    @IBOutlet private weak var descriptionRepository: UILabel?
    @IBOutlet private weak var imageRepositoryOwner: UIImageView?
    
    @IBOutlet private weak var nameOwner: UILabel?
    
    
    var repository : Repository? {
        didSet {
            nameRepository?.text        = repository?.name
            descriptionRepository?.text = repository?.description
            
            if let str = repository?.owner.avatarUrl, let url = URL(string: str) {
                imageRepositoryOwner?.sd_setImage(with: url)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}
