//
//  Owner.swift
//  Gits
//
//  Created by Diego Cavalcante on 18/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import Foundation


struct Owner {
    var name        : String?
    var fullName    : String?
    var avatarUrl   : String?
}
