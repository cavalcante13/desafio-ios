//
//  StringExtension.swift
//  Gits
//
//  Created by Diego Cavalcante on 20/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import Foundation


extension String {
    
    func int()-> Int {
        return Int(self) ?? 0
    }
}
