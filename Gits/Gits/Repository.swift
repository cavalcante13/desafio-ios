//
//  Repository.swift
//  Gits
//
//  Created by Diego Cavalcante on 18/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import Foundation

struct Repository {
    var id              = 0
    var totalResult     = 0
    var name            : String?
    var description     : String?
    var pullRequestUrl  : String?
    var owner           = Owner()
    
    var items           = [Repository]()
    
    init() {}
    
    init?(data : Any) {
        
        if let json = data as? JSON, let items = json["items"] as? [JSON] {
            totalResult = json["total_count"] as? Int ?? 0
            
            for item in items {
                var repo            = Repository()
                var owner           = Owner()
                repo.id             = item["id"]            as? Int    ?? 0
                repo.name           = item["name"]          as? String ?? nil
                repo.description    = item["description"]   as? String ?? nil
                
                if let pull = item["pulls_url"] as? String {
                    repo.pullRequestUrl = pull.replacingOccurrences(of: "{/number}", with: "/")
                }
                
                if let ownerJson    = item["owner"] as? [String : Any] {
                    owner.avatarUrl = ownerJson["avatar_url"] as? String ??  nil
                }
                repo.owner = owner
                self.items.append(repo)
            }
        }
    }
}
