//
//  RepositoryViewController.swift
//  Gits
//
//  Created by Diego Cavalcante on 16/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import UIKit

final class RepositoryViewController: UITableViewController {
    fileprivate var repository  = Repository()
    fileprivate var items       = [Repository]()
    fileprivate var pagination  = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.repository.github()
        setup()
        register()
        loadRepository()
    }
    
    final fileprivate func setup() {
        tableView.backgroundColor       = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        tableView.separatorStyle        = .singleLine
        tableView.rowHeight             = UITableViewAutomaticDimension
        tableView.estimatedRowHeight    = 44
        tableView.sectionFooterHeight   = 0.0
        tableView.tableFooterView       = UIView()
    }
    
    final fileprivate func register() {
        tableView.register(R.nib.repositoryTableViewCell)
        tableView.register(R.nib.loadMoreTableViewCell)
    }
    
    final fileprivate func loadRepository() {
        AppStyle.startActivityIndicator()
        let service : RestableService<Repository> = RestableService(path: Config.repositoryUrl(pagination: pagination))
        service.get(parse: Repository.init, callback: repositoryClosure())
    }
    
    final fileprivate func repositoryClosure()-> (Any?)-> Void {
        return { [weak self] repo in
            if let context = self {
                if let repository = repo as? Repository {
                    context.repository = repository
                    context.items      += repository.items
                    context.tableView.reloadData()
                }else if let error = repo as? Error {
                    context.confirmAlert(title: "Erro", description: error.localizedDescription, action: nil)
                    print(error)
                }
                AppStyle.stopActivityIndicator()
            }
        }
    }
    
    final fileprivate func loadMoreRepository() {
        pagination += 1
        loadRepository()
    }
}

extension RepositoryViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pagination < repository.totalResult ? (items.count + 1) : items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item < items.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.repositoryTableViewCell.identifier, for: indexPath) as! RepositoryTableViewCell
            cell.repository = items[indexPath.row]
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.loadMoreTableViewCell.identifier, for: indexPath)
            loadMoreRepository()
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(items[indexPath.item])
        let pullRequestViewController = PullRequestViewController(repository: items[indexPath.item])
        navigationController?.pushViewController(pullRequestViewController, animated: true)
    }
}










