//
//  PullRequestViewController.swift
//  Gits
//
//  Created by Diego Cavalcante on 20/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import UIKit

class PullRequestViewController: UITableViewController {
    
    private var repository  : Repository
    private var pullRequest = PullRequest()
    private var pagination  = 1
    
    init(repository : Repository) {
        self.repository = repository
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        register()
        loadPullRequests()
    }

    final fileprivate func setup() {
        tableView.backgroundColor       = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        tableView.separatorStyle        = .singleLine
        tableView.rowHeight             = UITableViewAutomaticDimension
        tableView.estimatedRowHeight    = 44
        tableView.sectionFooterHeight   = 0.0
        tableView.tableFooterView       = UIView()
    }
    
    /*
     loadPullRequests()
     repository.pullRequestUrl
    */
    final fileprivate func loadPullRequests() {
        AppStyle.startActivityIndicator()
        let service : RestableService<PullRequest> = RestableService(path: repository.pullRequestUrl?.appending(pagination.str()) ?? "")
        service.get(parse: PullRequest.init, callback: { [weak self] pull in
            if let context = self {
                if let pullRequest = pull as? PullRequest {
                    context.pullRequest = pullRequest
                    context.tableView.reloadData()
                }else if pull == nil {
                    context.confirmAlert(title: "Erro", description: R.string.pullRequest.repositórioNãoContémPullRequest(), action: nil)
                }else if let error = pull as? Error {
                    context.confirmAlert(title: "Erro", description: error.localizedDescription, action: nil)
                }
                AppStyle.stopActivityIndicator()
            }
        })
    }
    
    final fileprivate func register() {
        tableView.register(R.nib.repositoryTableViewCell)
        tableView.register(R.nib.loadMoreTableViewCell)
    }

}
