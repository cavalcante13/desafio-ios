//
//  Config.swift
//  Gits
//
//  Created by Diego Cavalcante on 19/06/17.
//  Copyright © 2017 Diego Cavalcante. All rights reserved.
//

import Foundation


struct Config {
    static func repositoryUrl(pagination : Int)-> String {
        return R.string.config.httpsApiGithubComSearchRepositoriesQLanguageJavaSortStarsPageD(pagination)
    }
}
